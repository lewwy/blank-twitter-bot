// Put your own Twitter App keys here. See README.md for more detail.
module.exports = {
	consumer_key: '',
	consumer_secret: '',
	access_token: '',
	access_token_secret: '',

	// Required to prevent the account from tweeting itself.
	// Ommit the @ symbol.
	account: 'AccountHandle',

	// List of accounts that are NOT retweeted. 
	// Omit the @ symbol.
	blocked: [
	  	
	],
	// Array of accounts who can trigger a stop on the bot with a tweet, and possibly issue other commands.
	masters: [],

	// Object of responses that the bot is capable of. New objects can be added, to facilitate different searches.
	responses: {
	    followed: [
	        "@#NAME Thanks for the follow. :)",
	        "@#NAME Thanks for the follow! Anything I can help with?",
	        "@#NAME Thanks for the follow! :)",
	        "@#NAME Really appreciate the follow! Can I help with something?",
	        "@#NAME Thanks for the follow. How can I help?"
	    ],
	    reply: [
	        "@#NAME I might be able to help - I've been doing it for around ten years. :)",
	        "@#NAME If you haven't got this sorted yet, I might be able to help :)",
	        "@#NAME If you're still looking, I might be able to help. Let me know if you want to discuss.",
	        "@#NAME Are you still looking for help with this?",
	        "@#NAME Are you still looking for help with this? Let me know if you want to discuss.",
	        "@#NAME I'd love to help. Can you explain a little bit more about what you're looking for?"      
	    ],
	    hosting: [
	    	'@#NAME I\'ve been using Dreamhost for 10 years, best I\'ve ever seen and it\'s easy to set up - check them out: http://lewis.so/dh',
			'@#NAME If you\'re looking something reliable, best support I\'ve ever had and easy to set up - check them out: http://lewis.so/dh',
			'@#NAME I\’ve been with Dreamhost for over 10 years and their support is second to none – check them out: http://lewis.so/dh',
			'@#NAME it might be worth looking into Dreamhost, I\’ve had 10 years great service – check them out: http://lewis.so/dh',
			'@#NAME I\’ve used Dreamhost for 10 years, never had a problem, amazing support team – http://lewis.so/dh',
			'@#NAME I recommend Dreamhost, amazing support team, great price, easy setup – http://http://lewis.so/dh'
	    ]
	},

	// Object containing all the data for the replies and retweets.
	searches: {
		reply: {
			// e.g. geocode:51.444,0.4559,20km
			geo: 'lang:en',
			negative: '-are -you -agency -rt -via -tattoo -interior -java -hiring -what -join -team -job -dress -costume -jewelery -jewellery -training -program -tutorial -role -in -free -cheap -hosting -host',
			people: 'anybody OR anyone OR someone OR somebody',
			terms: [
				'know OR recommend a web designer OR developer',
				'know OR recommend a wordpress dev OR developer OR programmer',
				'know OR recommend a graphic designer OR illustrator',
				'know OR recommend a SEO or PPC'
			]
		},
		hosting: {
			// e.g. geocode:51.444,0.4559,20km
			geo: 'lang:en',
			negative: '-free',
			people: 'anyone OR anybody OR someone OR somebody',
			terms: [
				'know OR recommend web OR website host OR hosting OR vps'
			]
		},
		retweet: { 
			q: "-cunt -burn -burned -shit -fuck -dick -tits -arse -fucking #illustration", 
			count: 3, 
			result_type: "recent"
		}
	}
}