// Rate Limits
// 15 Minute Windows
// Rate limits are divided into 15 minute intervals. Additionally, all endpoints require authentication, so there is no concept of unauthenticated calls and rate limits.

// There are two initial buckets available for GET requests: 15 calls every 15 minutes, and 180 calls every 15 minutes.

// Search
// Search is rate limited at 180 queries per 15 minute window.

// Debug flag
console.log("bot started: " + Date());
var debug = false;
var dev_mode = false;

// Our Twitter library
var Twit = require('twit');
var _ = require('underscore');
var fs = require('fs');
var config = require('./config.js');
var nodemailer = require('nodemailer');

// We need to include our configuration file
var T = new Twit(config);

// Account variables
var account = config.account,
    blocked = config.blocked;

/**
 * Simple function to return desired hours as milliseconds.
 * @param  {Integer} hours Desired number of hours
 */
function returnHours(hours) {
  return 1000 * 60 * 60 * hours;
}

function is_true(test) {
  if (test === true) {
    return true;
  } else {
    return false;
  }
}

/**
 * Used for the month specific advertisement tweets.
 * @return {string} Returns the current month as a string.
 */
function returnMonth() {
  var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
  ];

  var d = new Date();

  return monthNames[d.getMonth()];
}

function mail() {
  // create reusable transporter object using the default SMTP transport
  var transporter = nodemailer.createTransport('smtps://email:pass@hostdomain');

  // setup e-mail data with unicode symbols
  var mailOptions = {
      from: '"' + config.account + ' Twitter" <vps@creativeammo.co.uk>', // sender address
      to: 'email@domain.co.uk', // list of receivers
      subject: 'Corrupt DB', // Subject line
      text: 'I\'m feeling down.', // plaintext body
      html: '<b>I\'m feeling down.</b>' // html body
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, function(error, info){
      if(error){
          return console.log(error);
      }
      console.log('Message sent: ' + info.response);
  });
}


function returnFiles(directory) {
  var directory = fs.readdirSync(directory);
  return directory;
}

function getFilesizeInBytes(filename) {
 var stats = fs.statSync(filename);
 var fileSizeInBytes = stats["size"];
 return fileSizeInBytes;
}

function backup(file) {
  var d = new Date();
  var file_name = 'db_' + d.getYear() + d.getMonth() + d.getDate() + d.getHours() + d.getMinutes() + d.getSeconds() + '.json';
  fs.createReadStream(file).pipe(fs.createWriteStream('logs/' + file_name));
  console.log("db.json backed up");
}

/**
 * Returns true or false if a username is found in a list of blocked users.
 * @param  {string}   username      Username to be checked.
 * @param  {array}    blocked_list  Array of blocked usernames.
 * @return {Boolean}              
 */
function is_blocked(username, blocked_list) {
  var check = blocked_list.indexOf(username);
  if (check != -1) {
    return true;
  } else {
    return false;
  }
}

/**
 * Checks to see if the bot has already tweeted the user. We try to avoid spam where possible!
 * @param  {string} username Account username
 * @param  {array} list     Array of usernames to check against
 * @return {boolean}          true/false depending if username is found.
 */
function tweetedAlready(username, list) {
  var already_tweeted = false;
  for (var k in list) {
    if (!list.hasOwnProperty(k)) continue;
    if (list[k].user === username) {
      already_tweeted = true;
    }
  }
  return already_tweeted;
}

/**
 * Return a random response from the response object.
 * @param  {sting} category   Pass in the specific array of responses you want to return from
 * @param  {string} screenName In some cases the function will need a screen name to include.
 */
function randomResponse(category, screenName) {
    var response = config.responses[category][Math.floor(Math.random() * config.responses[category].length)];
    response = response.replace("#NAME", screenName);
    return response;
}

// A user stream
var stream = T.stream('statuses/filter', { track : ['@' + account] } );
var meStream = T.stream('user');
// When someone follows the user
meStream.on('follow', followed);
meStream.on('unfollow', unfollowed);

// In this callback we can see the name and screen name
function followed(event) {
  var name = event.source.name;
  var screenName = event.source.screen_name;
  if (screenName != config.account) {
    var response = randomResponse('followed', screenName);
      // Post the tweet 30 minutes later, for realism!
      // setTimeout(function(){
      //   T.post('statuses/update', { status: response }, tweeted);
      // }, returnHours(0.5));
    
      // Add a message to the node console.
      console.log('I was followed by: ' + name + ' @' + screenName);
  }
}

/**
 * In the event that someone unfollows, just log it to the node console.
 */
function unfollowed(event) {
  var name = event.source.name;
  var screenName = event.source.screen_name;

  if (screenName != config.account) {
    console.log('I was unfollowed by: ' + name + ' @' + screenName);
  }
}

/**
 * In an attempt to promote realism, this function searches for a specific criteria, and then retweets the most recent addition.
 */
function retweetLatest() {
  T.get('search/tweets', config.searches.retweet, function(error, data) {

    var recently_tweeted = false;

    var tweets = data.statuses;
    for (var i = 0; i < tweets.length; i++) {
      console.log(tweets[i].text);
    }


    // If our search request to the server had no errors...
    if (!error) {
      // ...then we grab the ID of the tweet we want to retweet...
      
      // Loop through possibles.
      var i = 0;

      data.statuses.forEach(function(){

        var retweetId = data.statuses[i].id_str;
        // ...and then we tell Twitter we want to retweet it!
        if (!is_blocked(data.statuses[i].user.screen_name, config.blocked) && !dev_mode && typeof (data.statuses[i].retweeted_status == "undefined") && !recently_tweeted) {
            console.log("Retweeting - " + data.statuses[i].user.screen_name);
            T.post('statuses/retweet/' + retweetId, {}, tweeted);
            recently_tweeted = true;
        }

      i++;

      });
    }

    // However, if our original search request had an error, we want to print it out here.
    else {
      if (debug) {
        console.log('There was an error with your hashtag search:', error);
      }
    }
  });
}

/**
 * Slightly more complex, this function searches twitter and then fires off responses to the users.
 */
function replySearch() {
  var timeout = 0;
  config.searches.reply.terms.forEach(function(index){
    setTimeout(function(){
      var jobTweets = { q: config.searches.reply.people + " " + index + " " + config.searches.reply.negative + " " + config.searches.reply.geo, count: 1, result_type: "recent" };
    
      //console.log(jobTweets);
      
      T.get('search/tweets', jobTweets, function(error, data) {
        var recently_tweeted = false;

        var tweets = data.statuses;

        var tweeted;
        var just_tweeted = [];

        try {
            tweeted = require('./db.json');
        } catch(e) {
          console.log("Nope: " + e);
            tweeted = {};
        }

        // Backup
        var backups = returnFiles('logs');
        if (backups.length > 10) {
          // Delete the oldest file.
          fs.unlink('logs/' + backups[0]);
        }

        if (getFilesizeInBytes('db.json') > 0) {
          backup('./db.json');
          var current_content = fs.readFileSync('./db.json', "utf-8");
          current_content = JSON.parse(current_content);

          var n = _.size(current_content)

          for (var i = 0; i < tweets.length; i++) {
            //console.log(tweets[i].user.screen_name);
            var screen_name = tweets[i].user.screen_name
            var reply = randomResponse('reply', screen_name);
            var params = { status: reply, in_reply_to_status_id: tweets[i].id_str}
            
            // console.log(params);

            tweeted[n] = {
              user: screen_name,
              original_tweet_id: tweets[i].id_str,
              reply: reply
            }

            n++;

            if (!tweetedAlready(screen_name, current_content) && just_tweeted.indexOf(screen_name) == -1) {
              console.log(params.status);
              // Actually send the tweets out
              T.post('statuses/update', params, function (err, data, response) {            
                console.log("SENT - " + screen_name)
              })
              just_tweeted.push(screen_name);
              fs.writeFile('./db.json', JSON.stringify(tweeted, null, 2), null);
            }
          }
        } else {
          // Email me an alert that it's gone tits up.
          mail();
        }
        // console.log(tweeted);
      });

    }, timeout * 5000);
    timeout++;
  })
}

/**
 * Return user information
 * @param  {string}   account  Target account's Twitter handle
 * @param  {Function} callback 
 */
function getUser(account, callback) {
  T.get('users/show', { user_id: account },  function (err, data, response) {
    callback(data);
  });
}

var timeout = 0;
function followFriendsOf(account, delay) {
  T.get('friends/ids', { screen_name: account },  function (err, data, response) {
    //console.log(data);
    data.ids.forEach(function(id, index){
      setTimeout(function(){
        //console.log(id);
        getUser(id, function(data) {
          if (!is_true(data.following) && !is_true(data.protected) && !is_true(data.default_profile) && !is_true(data.default_profile_image) && data.statuses_count > 50) {
            //console.log("Followed: " + data.screen_name);
            follow(id);
          } else {
            console.log("Not worth following:" + data.screen_name);
            console.log("Currently following: " + is_true(data.following));
            console.log("Account Protected: " + is_true(data.protected));
            console.log("Has Default Profile: " + is_true(data.default_profile));
            console.log("Has Default Profile Image: " + is_true(data.default_profile_image));
            console.log("Statuses: " + data.statuses_count);
          }
        });
      }, timeout * delay)
      timeout++;
    })
  });
}

/**
 * Post a random comment to twitter
 * @param  {string} category Pass through the type of message you want the function to select a random item from.
 */
function postRandom(category) {
    var response = randomResponse(category);

    // Post that tweet!
    if (!dev_mode) {
      T.post('statuses/update', { status: response }, tweeted);
    }
    console.log('Tweeted: ' + response);
}

// Make sure it worked!
function tweeted(err, reply) {
  if (err !== undefined) {
    console.log(err);
  } else {
    console.log('Tweeted: ' + reply);
  }
};

function follow(user_id) {
  T.post('friendships/create', {
    'user_id': user_id,
    'follow': true
  });

  console.log("Followed: " + user_id);
}

//followFriendsOf('businessessex', 1000 * 60 * 7);

// retweetLatest();
// setInterval(function(){
//   retweetLatest();
// }, returnHours(8));

replySearch('reply');
setInterval(function(){
  replySearch('reply');
}, returnHours(0.1));

replySearch('hosting');
setInterval(function(){
  replySearch('hosting');
}, returnHours(0.2));
